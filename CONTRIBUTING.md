# How to contribute

We're really glad you're reading this, because we welcome developers to help this project advance.

We want you working on things you're excited about.

[GitLab guide for First-Timers contributors](https://about.gitlab.com/blog/2016/06/16/fearless-contribution-a-guide-for-first-timers/)

Here are some important resources:

  * [HonestCode web](https://honestcode.io) info, blog and FAQs about HonestCode
  * [HonestCode service](https://pro.honestcode.io) fully working and ready to use web service
  * [HonestCode repository](https://gitlab.com/honestcode) the open source repositories.

## Issue tracking

Get involved in the project's issue tracker. To begin with, you may only create bug reports for issues you encounter. As you become more comfortable, consider triaging other bug reports.

Start by picking an existing issue and try to reproduce the problem in your own system. If you can reproduce it, outline the steps as a comment on the issue. This will save developer's time when they come to fix the bug later. If you cannot reproduce the issue, ask the reporter for more information. Triaging issues has a side-effect of helping you learn more about the project. This will come in handy as you contribute documentation and/or code in the future.

## Testing

ATDD's Gherkin tests made with Cucumber are available for almost all features (./test/acceptance/features), so please write tests for any change or addition.

## Submitting changes

Please send a Merge Request with a clear list of what you've done.
make sure all of your commits are atomic (one feature per commit).
When you send a Merge Request, please add tests for new features.

Always write a clear log message for your commits. One-line messages are fine for small changes, but bigger changes should look like this:

    $ git commit -m "A brief summary of the commit
    > 
    > A paragraph describing what changed and its impact."

## Feedback & Questions

Share feedback and ask questions on the HonestCode [Gitter Channel](https://gitter.im/honest-code)

For any other questions or feedback, please email to <honestcode@intelygenz.com>
