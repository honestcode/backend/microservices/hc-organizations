FROM node:12.13.0

EXPOSE 3000

RUN rm -rf /etc/service/sshd /etc/my_init.d/00_regen_ssh_host_keys.sh

ADD package.json package.json
RUN npm install
ADD . .

CMD ["npm","start"]
