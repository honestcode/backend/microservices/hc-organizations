[![HonestCode](https://pro.honestcode.io/api/blueprint/bp.4zLgQF2Xn/environment/TmHx4iukNN0qWOOjSC5pPcb2/badge.svg)](https://pro.honestcode.io/#/blueprint/edit/bp.4zLgQF2Xn)

# HonestCode.io Teams (Organizations) Microservice

This microservice manage the teams (aka organizations) of honestcode.io

You can create new organizations, edit the name or logo of existing organizations that you can access or remove them.

## Architecture

This micro service uses nodejs with mongodb to store data

## Tests


### Acceptance tests need a MongoDB and HTTP Server running

Start server
```
npm start
```
Run tests
```
npm run test-acceptance
```

Run tests and fail fast
```
npm run test-acceptance-fast
```

Run @only tagged tests
```
npm run test-acceptance-only
```

## Run server
```
npm start
```

## License
   Copyright 2020 Intelygenz Inc. & Intelygenz S.A.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
