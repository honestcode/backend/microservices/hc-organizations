'use strict';

const config = require('../config.js');
const MongoClient = require('mongodb').MongoClient;
const logger = require('../logger');

let isConnected = false;
let currentDb;

const collectionName = 'organizations';

const connOptions = {
	useNewUrlParser: true,
	useUnifiedTopology: true,
	retryWrites: false
};

module.exports = () =>
	new Promise((resolve, reject) => {
		if (isConnected) {
			return resolve(currentDb);
		}

		MongoClient.connect(config.DB_URI, connOptions, (err, client) => {
			if (err) {
				logger.error(`Error when try to connect to DB: ${err}`, {transactionId: 'db_start'});
				return reject(err);
			}
			logger.info('connected to database', {transactionId: 'db_start'});

			client.on('error', err => {
				logger.error(`db error: ${JSON.stringify(err)}`, {transactionId: 'db_error'});
			});

			client.on('close', () => {
				isConnected = false;
				logger.warn('db closed', {transactionId: 'db_close'});
			});

			client.on('timeout', err => {
				logger.error(`db timeout: ${JSON.stringify(err)}`, {transactionId: 'db_timeout'});
			});

			currentDb = client.db(config.DB_NAME);
			isConnected = true;

			currentDb.collection(collectionName).countDocuments()
				.then(count => {
					logger.info(`${count} ${collectionName} found on database`, {transactionId: 'db_start'});
				});

			resolve(currentDb);
		});
	});
