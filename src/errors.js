'use strict';

module.exports = {
	ORGANIZATION_ALREADY_EXIST: {
		err: 'invalid_organization',
		des: 'team with same name already exists'
	},
	ORGANIZATION_REQUIRE_NAME: {
		err: 'invalid_organization',
		des: 'team must have a name'
	},
	ORGANIZATION_REQUIRE_AVATAR: {
		err: 'invalid_organization',
		des: 'team must have an avatar'
	},
	ORGANIZATION_INVALID_MEMBERS: {
		err: 'invalid_members',
		des: 'no members given'
	},
	ORGANIZATION_DOES_NOT_EXIST: {
		err: 'invalid_organization',
		des: 'team does not exist'
	},
	ORGANIZATION_INVALID_NAME_CHARACTERS: {
		err: 'invalid_organization',
		des: 'invalid character on name'
	},
	MEMBER_DOES_NOT_EXIST: {
		err: 'invalid_member',
		des: 'member does not exist'
	},
	NOTHING_TO_UPDATE: {
		err: 'invalid_request',
		des: 'nothing to update'
	},
	ONE_ADMIN_MUST_REMAIN: {
		err: 'invalid_request',
		des: 'team must remain with at least one admin'
	}
};
