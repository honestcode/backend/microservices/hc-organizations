'use strict';

const os = require('os');
const winston = require('winston');
const papertrail = require('winston-papertrail').Papertrail;
const semver = require('semver');
const packageVersion = require('../package.json').version;
const config = require('./config');

const consoleLogger = new winston.transports.Console({
	level: 'error',
	timestamp: () => {
		return new Date().toISOString();
	},
	colorize: false
});

const version = semver.valid(packageVersion) ? semver.patch(packageVersion) : 'dev';

const ptTransport = new papertrail({
	host: config.papertrail.host,
	port: config.papertrail.port,
	hostname: os.hostname(),
	program: `backend-organizations-${version}`,
	level: 'error',
	logFormat: (level, message) => {
		return `[${level}] ${message}`;
	},
	handleExceptions: false,
	colorize: true,
	inlineMeta: true
});

ptTransport.on('error', function (err) {
	if (logger) logger.error(err, {transactionId: 'papertrail'});
});

ptTransport.on('connect', function (message) {
	if (logger) logger.info(message, {transactionId: 'papertrail'});
});

// //winston-papertrail is incompatible with winston 3
// const winston3Format = winston.format.printf( info => {
// 	try {
// 		let transactionId;
// 		if (!info || !info.transactionId) {
// 			transactionId = 'transactionId required on log';
// 		}
// 		else{
// 			transactionId = info.transactionId
// 		}
// 		return `[${info.level}] ${info.message} {transactionId: '${transactionId}'}`
// 	}
// 	catch (err){
// 		  return ('logger error: '+JSON.stringify(err));
// 	}
// })


const logger = new winston.Logger({
	levels: {
		debug: 0,
		info: 1,
		warn: 2,
		error: 3
	},
	transports: [
		ptTransport,
		consoleLogger
	],
	rewriters: [
		(level, message, meta) => {
			if (!meta || !meta.transactionId) {
				return {transactionId: 'transactionId required on log'};
			}
			return {
				transactionId: meta.transactionId
			};
		}
	]
});

module.exports = logger;
