'use strict';

const storageConnection = require('../database/connection.js');
const shortid = require('shortid');
const errors = require('../errors');
const find = require('./find');
const config = require('../config.js');

module.exports = (name, avatar, creatorId) => {

	const organization = {
		id: `or.${shortid.generate()}`,
		name,
		members: [{
			id: creatorId,
			isAdmin: true
		}],
		creationDate: new Date(),
		creator: creatorId,
		licenses: {free: config.LICENSES_FREE }
	};


	if (avatar) {
		organization.avatar = avatar;
	}

	return new Promise((ok, ko) => {
		const uniqueNameFilter = {
			name: new RegExp(`^${organization.name}$`, 'i')
		};

		find(uniqueNameFilter)
			.then(organizations => {
				if (organizations.length > 0) {
					return ko(errors.ORGANIZATION_ALREADY_EXIST);
				}
				return storageConnection();
			})
			.then(db => db.collection('organizations').insertOne(organization))
			.then(() => ok(organization))
			.catch(err => ko(err));
	});
};
