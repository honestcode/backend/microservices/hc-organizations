'use strict';

const create = require('./create.js');
const errors = require('../errors');
const logger = require('../logger');
const isValidName = require('../utils/isValidName');

module.exports = (req, res, next) => {
	logger.info(`attempt to create an organization with params ${JSON.stringify(req.params)}`, req);

	if (!req.params.name) {
		logger.warn('attempt to create an organization without name', req);
		res.send(400, errors.ORGANIZATION_REQUIRE_NAME);
		return next();
	}

	if (!isValidName(req.params.name)) {
		logger.warn('invalid characters found on name', req);
		res.send(409, errors.ORGANIZATION_INVALID_NAME_CHARACTERS);
		return next();
	}

	create(req.params.name, req.params.avatar, req.params.creatorId)
		.then(organization => {
			res.send(201, organization);
			return next();
		})
		.catch(err => {
			if (err === errors.ORGANIZATION_ALREADY_EXIST) {
				res.send(409, err);
			} else {
				logger.error(`error when creating an organization ${err}`, req);
				res.send(500, err);
			}
			return next();
		});
};
