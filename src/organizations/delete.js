'use strict';

const storageConnection = require('../database/connection.js');
const logger = require('../logger');

module.exports = (organizationId, transaction) => {
	return new Promise((resolve, reject) => {

		logger.info(`attempt to delete the organization ${organizationId}`, transaction);

		storageConnection()
			.then(db => db.collection('organizations').updateOne({id: organizationId}, {$set: {deleted: true}}))
			.then(() => resolve())
			.catch(err => reject(err));
	});
};
