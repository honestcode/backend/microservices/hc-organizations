'use strict';

const organizationsDelete = require('./delete.js');

module.exports = (req, res, next) => {

	organizationsDelete(req.params.organizationId, req)
		.then(() => {
			res.send(204);
			next();
		})
		.catch(err => {
			res.send(500, err);
			next();
		});

};
