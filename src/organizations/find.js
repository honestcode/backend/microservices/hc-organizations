'use strict';

const storageConnection = require('../database/connection.js');

module.exports = query => {
	const s = query ? null : {name: 1};
	const q = Object.assign({deleted: {$exists: false}}, query);

	return new Promise((resolve, reject) => {
		storageConnection()
			.then(db => db.collection('organizations').find(q).sort(s).project({_id: 0}))
			.then(cursor => {
				resolve(cursor.toArray());
			})
			.catch(err => reject(err));
	});
};
