'use strict';

const storageConnection = require('../database/connection.js');
const logger = require('../logger');

module.exports = (memberId, req) => {

	logger.info(`finding organization for member ${memberId}`, req);

	const q = {'members.id': memberId, deleted: {$exists: false}};

	return new Promise((resolve, reject) => {
		storageConnection()
			.then(db => db.collection('organizations').find(q).project({_id: 0}))
			.then(cursor => {
				resolve(cursor.toArray());
			})
			.catch(err => reject(err));
	});
};
