'use strict';

const storageConnection = require('../database/connection.js');

module.exports = name => {

	const q = {name, deleted: {$exists: false}};

	return new Promise((resolve, reject) => {
		storageConnection()
			.then(db => db.collection('organizations').find(q).project({_id: 0}))
			.then(cursor => {
				resolve(cursor.toArray());
			})
			.catch(err => reject(err));
	});
};
