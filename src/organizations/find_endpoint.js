'use strict';

const find = require('./find');
const logger = require('../logger');

module.exports = (req, res, next) => {

	if (req.debug) logger.info(`finding organizations for query params: ${JSON.stringify(req.params)}`, req);

	const memberId = req.params.q && req.params.q.memberId;
	const query = memberId ? {'members.id': memberId} : null;

	find(query)
		.then(organizations => {
			res.send(200, {items: organizations});
			return next();
		})
		.catch(err => {
			logger.error(`error finding organizations: ${JSON.stringify(err)}`, req);
			res.send(500, {err: 'internal_error', des: 'error finding organizations'});
			return next();
		});
};
