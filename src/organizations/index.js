'use strict';

module.exports = server => {
	server.post('/api/organization', require('./create_endpoint'));
	server.get('/api/organizations', require('./find_endpoint'));
	server.get('/api/organization/:organizationId', require('./retrieve_endpoint'));
	server.put('/api/organization/:organizationId', require('./update_endpoint'));
	server.del('/api/organization/:organizationId', require('./delete_endpoint'));
	// MEMBERS
	server.post('/api/organization/:organizationId/members', require('./members_add_endpoint'));
	server.put('/api/organization/:organizationId/member/:memberId', require('./member_update_endpoint'));
	server.del('/api/organization/:organizationId/member/:memberId', require('./member_delete_endpoint'));
};
