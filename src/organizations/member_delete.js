'use strict';

const storageConnection = require('../database/connection.js');
const logger = require('../logger');

module.exports = (organizationId, memberId, transaction) => {
	return new Promise((resolve, reject) => {

		logger.info(`attempt to delete member ${memberId} from organization ${organizationId}`, transaction);

		storageConnection()
			.then(db => db.collection('organizations').updateOne({id: organizationId}, {$pull: {members: {id: memberId}}}))
			.then(() => resolve())
			.catch(err => reject(err));
	});
};
