'use strict';

const organizationsMemberDelete = require('./member_delete');

module.exports = (req, res, next) => {

	organizationsMemberDelete(req.params.organizationId, req.params.memberId, req)
		.then(() => {
			res.send(204);
			next();
		})
		.catch(err => {
			res.send(500, err);
			next();
		});

};
