'use strict';

const storageConnection = require('../database/connection.js');
const retrieve = require('./retrieve');

module.exports = (organizationId, memberId, updateData) => {
	return new Promise((ok, ko) => {
		storageConnection()
			.then(db => db.collection('organizations').updateOne({
				id: organizationId,
				'members.id': memberId
			}, updateData))
			.then(() => retrieve(organizationId))
			.then(ok)
			.catch(err => ko(err));
	});
};
