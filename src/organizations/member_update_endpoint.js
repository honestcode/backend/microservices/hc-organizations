'use strict';

const retrieve = require('./retrieve');
const memberUpdate = require('./member_update');
const logger = require('../logger');
const errors = require('../errors');

module.exports = (req, res, next) => {

	logger.info(`attempt to change the role of an organization member with params ${JSON.stringify(req.params)}`, req);

	retrieve(req.params.organizationId)
		.then(organization => {
			if (!organization) {
				res.send(404, errors.ORGANIZATION_DOES_NOT_EXIST);
				return Promise.reject();
			}

			const member = organization.members.find(member => member.id === req.params.memberId);
			if (!member) {
				res.send(404, errors.MEMBER_DOES_NOT_EXIST);
				return Promise.reject();
			}

			if (Object.prototype.hasOwnProperty.call(req.params, 'isAdmin') && req.params.isAdmin === false) {
				if (member.isAdmin === req.params.isAdmin) {
					res.send(409, errors.NOTHING_TO_UPDATE);
					return Promise.reject();
				}

				const currentAdmins = organization.members.filter(member => member.isAdmin === true);
				if (member.isAdmin && currentAdmins.length === 1) {
					res.send(409, errors.ONE_ADMIN_MUST_REMAIN);
					return Promise.reject();
				}
			}

			const updateData = {
				$set: {},
				$unset: {}
			};
			if (Object.prototype.hasOwnProperty.call(req.params, 'isAdmin')) {
				if (req.params.isAdmin) {
					updateData.$set['members.$.isAdmin'] = true;
				} else {
					updateData.$unset['members.$.isAdmin'] = false;
				}
			}
			if (Object.keys(updateData.$set).length === 0) delete(updateData.$set);
			if (Object.keys(updateData.$unset).length === 0) delete(updateData.$unset);
			if (Object.keys(updateData).length === 0) {
				res.send(400, errors.NOTHING_TO_UPDATE);
				return Promise.reject();
			}

			return memberUpdate(organization.id, member.id, updateData);
		})
		.then(doc => {
			res.send(200, doc);
			next();
		})
		.catch(err => {
			if (err) {
				logger.error(`error trying to change member role ${err}`, req);
				res.send(500, {err: 'internal_error', des: 'error trying to change member role'});
			}
			next();
		});
};
