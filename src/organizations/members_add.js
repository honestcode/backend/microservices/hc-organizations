'use strict';

const storageConnection = require('../database/connection.js');
const retrieve = require('./retrieve');
const Int32 = require('mongodb').Int32;
const moment = require('moment');

module.exports = (organizationId, newMembers) => {

	const membersToAdd = newMembers.map(newMember => {
		const memberToAdd = {
			id: newMember.id
		};
		if(newMember.isAdmin){
			memberToAdd.isAdmin = true;
		}
		return memberToAdd;
	});

	const update = {
		$push: {
			members: {
				$each: membersToAdd
			}
		},
		$set: {
			lastInviteDate: new Date()
		}
	};

	return new Promise((resolve, reject) => {
		retrieve(organizationId)
			.then(organization => {
				const lastInviteDate = moment(organization.lastInviteDate || null);
				const limitedPeriod = moment().subtract(24, 'hours'); // Must match the one defined in backend-apiproxy/src/permissions/ensure_has_available_licenses.js

				if (lastInviteDate.isBefore(limitedPeriod)) {
					update.$set.invitesCount = 0;
				} else {
					update.$inc = { invitesCount: Int32(membersToAdd.length) };
				}
			})
			.then(() => storageConnection())
			.then(db => db.collection('organizations').updateOne({id: organizationId}, update))
			.then(() => resolve())
			.catch(err => reject(err));
	});

};
