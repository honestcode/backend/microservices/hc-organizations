'use strict';

const organizationsAddMembers = require('./members_add.js');
const logger = require('../logger');
const errors = require('../errors');

module.exports = (req, res, next) => {

	if (req.debug) logger.info(`attempt to add members ${JSON.stringify(req.body.members)} to organization ${req.params.organizationId}`, req);

	if (!req.params.members || req.params.members.length === 0) {
		logger.warn('no members given', req);
		res.send(400, errors.ORGANIZATION_INVALID_MEMBERS);
		return next();
	}

	organizationsAddMembers(req.params.organizationId, req.params.members)
		.then(() => {
			res.send(201, {});
			return next();
		})
		.catch(err => {
			res.send(500, err);
			return next();
		});
};
