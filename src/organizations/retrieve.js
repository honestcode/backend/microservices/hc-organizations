'use strict';

const storageConnection = require('../database/connection.js');

module.exports = organizationId => {
	return new Promise((resolve, reject) => {
		storageConnection()
			.then(db => db.collection('organizations').find({id: organizationId}).project({_id: 0}).limit(1))
			.then(cursor => {
				cursor.next((err, organizationDoc) => {
					if (err) return reject(err);
					return resolve(organizationDoc);
				});
			})
			.catch(err => reject(err));
	});
};
