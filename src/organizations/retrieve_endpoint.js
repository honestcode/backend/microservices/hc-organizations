'use strict';

const retrieve = require('./retrieve.js');
const logger = require('../logger');
const config = require('../config.js');

module.exports = (req, res, next) => {

	retrieve(req.params.organizationId)
		.then(organization => {
			if (organization) {
				if (!organization.licenses) {
					organization.licenses = {};
				}
				if (!organization.licenses.free) organization.licenses.free = config.LICENSES_FREE;
			}

			res.send(organization ? 200 : 404, organization);
			return next();
		})
		.catch(err => {
			logger.error(`error retrieving organization by id err:${JSON.stringify(err)}`, req);
			res.send(500, {err: 'internal_error', des: 'error retrieving organization by id'});
			return next();
		});

};
