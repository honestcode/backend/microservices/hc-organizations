'use strict';

const retrieve = require('./retrieve');
const errors = require('../errors');
const update = require('./update');
const logger = require('../logger');
const find = require('./find');
const isValidName = require('../utils/isValidName');

module.exports = (req, res, next) => {
	logger.info(`attempt to update a organization with params ${JSON.stringify(req.params)}`, req);

	if (!isValidName(req.params.name)) {
		logger.warn('invalid characters found on name', req);
		res.send(409, errors.ORGANIZATION_INVALID_NAME_CHARACTERS);
		return next();
	}

	retrieve(req.params.organizationId)
		.then(organization => {
			if (!organization) {
				res.send(404, errors.ORGANIZATION_DOES_NOT_EXIST);
				return next();
			}

			if (Object.prototype.hasOwnProperty.call(req.params, 'name')) {
				const uniqueNameFilter = {
					id: {$ne: organization.id},
					name: new RegExp(`^${req.params.name}$`, 'i')
				};
				return find(uniqueNameFilter);
			}
		})
		.then(sameNameOrganizations => {
			if (sameNameOrganizations && sameNameOrganizations.length > 0) {
				res.send(409, errors.ORGANIZATION_ALREADY_EXIST);
				return Promise.reject(errors.ORGANIZATION_ALREADY_EXIST);
			}

			const updateData = {
				$set: {}
			};
			if (Object.prototype.hasOwnProperty.call(req.params, 'name')) updateData.$set.name = req.params.name;
			if (Object.prototype.hasOwnProperty.call(req.params, 'avatar')) updateData.$set.avatar = req.params.avatar;
			if (Object.prototype.hasOwnProperty.call(req.params, 'licenses')) updateData.$set['licenses.free'] = req.params.licenses.free;
			if (Object.keys(updateData.$set).length === 0) delete(updateData.$set);

			if (Object.keys(updateData).length > 0) {
				return update(req.params.organizationId, updateData);
			}
			return Promise.resolve();
		})
		.then(doc => {
			res.send(200, doc);
			return next();
		})
		.catch(err => {
			logger.error(`organization update endpoint: unknown error: ${JSON.stringify(err)}`, req);
			//			res.send(500, err);
			return next();
		});

};
