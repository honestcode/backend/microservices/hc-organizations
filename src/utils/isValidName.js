const pattern = /^[a-zA-Z0-9\s-_.()[\]{},;:+*=@#|'"$%&]+$/;

module.exports = name => pattern.test(name);
