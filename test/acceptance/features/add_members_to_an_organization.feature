Feature: Add members to an Organization
	As a User
	I want to be able to add new members to an Organizations

	Scenario: Add new members to a team successfully
		Given I am the user 1
		When I request creation of new organization with following data
			| name        | avatar                    |
			| Tyrell Corp | http://hc-orgs/tyrell.jpg |
		And I request to add the following email "1@1.com" as new member to organization "Tyrell Corp"
		Then the response status code is 201

	Scenario: Check added members
		Given I am the user 1
		When I request creation of new organization with following data
			| name        | avatar                    |
			| Tyrell Corp | http://hc-orgs/tyrell.jpg |
		And I request to add the following email "1@1.com" as new member to organization "Tyrell Corp"
		And I request to add the following email "2@2.com" as new member to organization "Tyrell Corp"
		And I request the organization "Tyrell Corp"
		Then the response status code is 200
		And the response body has a "members" field with 3 items
		And the response body member 0 has a "isAdmin" field with true value
		And the response body member 1 does not has a "isAdmin" field
		And the response body member 2 does not has a "isAdmin" field

	#si no existe el usuario
	#si ya existe el usuario en el team

# originales (ya no deberian funcionar o que admita ambas)
	Scenario: Add new members to an organization successfully
		Given I am the user 1
		When I request creation of new organization with following data
			| name        | avatar                    |
			| Tyrell Corp | http://hc-orgs/tyrell.jpg |
		And I request to add the following users as new members to organization "Tyrell Corp"
			| user   | isAdmin |
			| user2  | true    |
			| user33 | false   |
		Then the response status code is 201

	Scenario: Check added admin member
		Given I am the user 1
		When I request creation of new organization with following data
			| name        | avatar                    |
			| Tyrell Corp | http://hc-orgs/tyrell.jpg |
		And I request to add the following users as new members to organization "Tyrell Corp"
			| user  | isAdmin |
			| user2 | true    |
			| user3 | false   |
		And I request the organization "Tyrell Corp"
		Then the response status code is 200
		And the response body has a "members" field with 3 items
		And the response body member 0 has a "isAdmin" field with true value
		And the response body member 1 has a "isAdmin" field with true value
		And the response body member 2 does not has a "isAdmin" field

	Scenario: No members given
		Given I am the user 1
		When I request creation of new organization with following data
			| name        | avatar                    |
			| Tyrell Corp | http://hc-orgs/tyrell.jpg |
		And I request to add the following users as new members to organization "Tyrell Corp"
			| user | isAdmin |
		Then the response status code is 400
		And the response body has a "err" field with a "invalid_members" value
		And the response body has a "des" field with a "no members given" value
