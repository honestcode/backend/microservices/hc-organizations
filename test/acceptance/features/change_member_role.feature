Feature: Change member role
	As an API client
	I want to be able to change the role of a member inside an organization

	Background: active user
		Given The following users exists
			| id     | name   |
			| user 1 | User 1 |
			| user 2 | User 2 |
			| user 3 | User 3 |
		And I am the user "User 1"

	Scenario Outline: Change role of a member has valid response
		When I request creation of new organization with following data
			| name           | avatar          |
			| Organization 1 | http://org1.jpg |
		And I request to add the following users as new members to organization "Organization 1"
			| user   | isAdmin |
			| user 2 | false   |
			| user 3 | true    |
		And I request to change the organization "Organization 1" member "<NAME>" role to admin
		Then the response status code is 200

		Examples:
			| NAME   |
			| User 2 |
			| User 3 |

	Scenario: Delete a concrete member of an organization successfully
		When I request creation of new organization with following data
			| name           | avatar          |
			| Organization 1 | http://org1.jpg |
		And I request to add the following users as new members to organization "Organization 1"
			| user   | isAdmin |
			| user 2 | false   |
			| user 3 | true    |
		And I request to change the organization "Organization 1" member "User 2" role to admin
		And I request to change the organization "Organization 1" member "User 3" role to editor
		And I request the organization "Organization 1"
		Then the response status code is 200
		And the response body has a "members" field with 3 items
		And the response body member 0 has a "isAdmin" field with true value
		And the response body member 1 has a "isAdmin" field with true value
		And the response body member 2 does not has a "isAdmin" field

	Scenario: At least one admin must remain
		When I request creation of new organization with following data
			| name           | avatar          |
			| Organization 1 | http://org1.jpg |
		And I request to add the following users as new members to organization "Organization 1"
			| user   | isAdmin |
			| user 2 | false   |
			| user 3 | false   |
		And I request to change the organization "Organization 1" member "User 1" role to editor
		Then the response status code is 409
		And the response body has a "err" field with a "invalid_request" value
		And the response body has a "des" field with a "team must remain with at least one admin" value

