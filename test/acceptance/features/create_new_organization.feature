Feature: Create new Organization
	As a User
	I want to be able to create new organizations

	Scenario: Create new organization successfully
		Given I am the user 1
		When I request creation of new organization with following data
			| name        | avatar                    |
			| Tyrell Corp | http://hc-orgs/tyrell.jpg |
		Then the response status code is 201
		And the response body has a "id" field

	Scenario: Created organization has correct information
		Given I am the user 1
		When I request creation of new organization with following data
			| name        | avatar                    |
			| Tyrell Corp | http://hc-orgs/tyrell.jpg |
		And I as member request to find all organizations
		Then the response status code is 200
		And the response body has a "items" field with 1 item
		And the response body item 0 has a "id" field
		And the response body item 0 has a "name" field
		And the response body item 0 has a "avatar" field
		And the response body item 0 has a "members" field with 1 item

	Scenario: Organization must have a name
		Given I am the user 1
		When I request creation of new organization with following data
			| avatar                    |
			| http://hc-orgs/tyrell.jpg |
		Then the response status code is 400
		And the response body has a "err" field with a "invalid_organization" value
		And the response body has a "des" field with a "team must have a name" value

	Scenario: Organization avatar is optional
		Given I am the user 1
		When I request creation of new organization with following data
			| name        |
			| Tyrell Corp |
		Then the response status code is 201
