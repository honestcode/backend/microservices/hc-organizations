Feature: Delete a member of an organization
	As a User
	I want to be able to delete a member an organization

	Scenario: Delete a concrete member of an organization
		Given I am the user 1
		When I request creation of new organization with following data
			| name        | avatar                    |
			| Tyrell Corp | http://hc-orgs/tyrell.jpg |
		And I request to delete the member with id 1 of the organization "Tyrell Corp"
		Then the response status code is 204

	Scenario: Delete a concrete member of an organization successfully
		Given I am the user 1
		When I request creation of new organization with following data
			| name        | avatar                    |
			| Tyrell Corp | http://hc-orgs/tyrell.jpg |
		And I request to delete the member with id 1 of the organization "Tyrell Corp"
		And I as member request to find all organizations
		Then the response status code is 200
		And the response body has a "items" field with 0 items

