Feature: Delete an organization
	As a User
	I want to be able to delete an organization

	Scenario: Delete a concrete organization successfully
		Given I am the user 1
		When I request creation of new organization with following data
			| name        | avatar                    |
			| Tyrell Corp | http://hc-orgs/tyrell.jpg |
		And I request to delete the organization "Tyrell Corp"
		Then the response status code is 204

	Scenario:  A deleted organization is unavailable
		Given I am the user 1
		When I request creation of new organization with following data
			| name        | avatar                    |
			| Tyrell Corp | http://hc-orgs/tyrell.jpg |
		And I request to delete the organization "Tyrell Corp"
		And I as member request to find all organizations
		Then the response status code is 200
		And the response body has a "items" field with 0 items

