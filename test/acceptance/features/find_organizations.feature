Feature: Find organizations
	As a User
	I want to be able to find organizations

	Scenario: Find all organizations by member
		Given I am the user 1
		And I request creation of new organization with following data
			| name         | avatar                     |
			| HonestCode | http://hc-orgs/hc.jpg |
		And I am the user 2
		And I request creation of new organization with following data
			| name         | avatar                     |
			| Tyrell Corp  | http://hc-orgs/tyrell.jpg  |
		And I request creation of new organization with following data
			| name         | avatar                     |
			| Tayland Ind | http://hc-orgs/wayland.jpg |
		When I as member request to find all organizations
		Then the response status code is 200
		And the response body has a "items" field with 2 items
		And the response body item 0 has a "name" field with "Tyrell Corp" value
		And the response body item 0 has a "avatar" field with "http://hc-orgs/tyrell.jpg" value

		Scenario: Find all organizations by admin
		Given I am the user 1
		And I request creation of new organization with following data
			| name         | avatar                |
			| HonestCode   | http://hc-orgs/hc.jpg |
		And I request creation of new organization with following data
			| name         | avatar                |
			| Nextinit   | http://hc-orgs/nxt.jpg |
		And I am the user 2
		And I request creation of new organization with following data
			| name         | avatar                     |
			| Tyrell Corp  | http://hc-orgs/tyrell.jpg  |
		And I request creation of new organization with following data
			| name         | avatar                     |
			| Wayland Ind | http://hc-orgs/wayland.jpg |
		When I as admin request to find all organizations
		Then the response status code is 200
		And the response body has a "items" field with 4 items
		And the response body item 0 has a "name" field with "HonestCode" value
		And the response body item 0 has a "avatar" field with "http://hc-orgs/hc.jpg" value

	Scenario: No organizations
		Given I am the user 1
		When I as member request to find all organizations
		Then the response status code is 200
		And the response body has a "items" field with 0 items
