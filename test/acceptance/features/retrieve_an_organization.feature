Feature: Retrieve an organization
	As a User
	I want to be able to retrieve a concrete organizations

	Scenario: Retrieve a concrete organization successfully
		Given I am the user 1
		When I request creation of new organization with following data
			| name        | avatar                    |
			| Tyrell Corp | http://hc-orgs/tyrell.jpg |
		When I request the organization "Tyrell Corp"
		Then the response status code is 200
		And the response body has a "name" field with a "Tyrell Corp" value
		And the response body has a "avatar" field with a "http://hc-orgs/tyrell.jpg" value
		And the response body has a "licenses" field with an inner "free" field
		And the response body has a "licenses.free" field equal to number "5"

	Scenario: Organization does not exist
		When I request the organization "Invalid Organization"
		Then the response status code is 404
