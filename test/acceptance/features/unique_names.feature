Feature: Unique names
	As an api client
	I must not be able to create or edit two organzations with the same name

	Background:
		Given I am the user 1
		When I request creation of new organization with following data
			| name           | avatar          |
			| organization 1 | http://org1.jpg |
		And I request creation of new organization with following data
			| name           | avatar          |
			| organization 2 | http://org2.jpg |

	Scenario: update same organization is ok
		And I request to edit the organization "organization 1" to the new data
			| name           | avatar            |
			| organization 1 | http://org1_2.jpg |
		Then the response status code is 200

	Scenario Outline: Create similar Team/Organization name works
		When I request creation of new organization with following data
			| name   | avatar          |
			| <NAME> | http://org1.jpg |
		Then the response status code is 201
		Examples:
			| NAME      |
			| organization 3 |
			| org       |
			| on 1      |
		  | niza      |

	Scenario Outline: Create same Team/Organization name fails
		When I request creation of new organization with following data
			| name   | avatar          |
			| <NAME> | http://org1.jpg |
		Then the response status code is 409
		And the response body has a "err" field with a "invalid_organization" value
		And the response body has a "des" field with a "team with same name already exists" value
		Examples:
			| NAME      |
			| organization 1 |
			| Organization 1 |
			| organizatioN 1 |
		  | orGaniZaTion 1 |

	Scenario Outline: update similar Team/Organization name works
		And I request to edit the organization "organization 2" to the new data
			| name   | avatar          |
			| <NAME> | http://org1.jpg |
		Then the response status code is 200
		Examples:
			| NAME      |
			| organization 3 |
			| org       |
			| on 1      |
		  | niza      |

	Scenario Outline: update same Team/Organization name fails
		And I request to edit the organization "organization 2" to the new data
			| name   | avatar          |
			| <NAME> | http://org1.jpg |
		Then the response status code is 409
		And the response body has a "err" field with a "invalid_organization" value
		And the response body has a "des" field with a "team with same name already exists" value
		Examples:
			| NAME      |
			| organization 1 |
			| Organization 1 |
			| organizatioN 1 |
		  | orGaniZaTion 1 |
