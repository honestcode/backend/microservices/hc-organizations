Feature: Update an organization
	As a api client
	I want to be able to update a concrete organization

	Scenario: Update a concrete organization successfully
		Given I am the user 1
		When I request creation of new organization with following data
			| name           | avatar          |
			| organization 1 | http://org1.jpg |
		And I request to edit the organization "organization 1" to the new data
			| name                | avatar            |
			| edited organization | http://org1_2.jpg |
		Then the response status code is 200
		And the response body has a "id" field

	Scenario: Updated organization has actually modified their fields
		Given I am the user 1
		When I request creation of new organization with following data
			| name           | avatar          |
			| organization 1 | http://org1.jpg |
		And I request to edit the organization "organization 1" to the new data
			| name                | avatar            |
			| edited organization | http://org1_2.jpg |
		When I request the organization "edited organization"
		Then the response status code is 200
		And the response body has a "name" field with a "edited organization" value
		And the response body has a "avatar" field with a "http://org1_2.jpg" value
@only
	Scenario: Update invalid organization
		When I request to edit the organization "invalid" to the new data
			| name                | avatar            |
			| edited organization | http://org1_2.jpg |
		Then the response status code is 404
		And the response body has a "err" field with a "invalid_organization" value
		And the response body has a "des" field with a "team does not exist" value

	Scenario: SuperAdmin updates organization licenses
		Given I am the user 1
		When I request creation of new organization with following data
			| name           | avatar          |
			| organization 1 | http://org1.jpg |
		And I request to edit the organization "organization 1" to the new data
			| name           | licenses |
			| organization 1 | 7        |
		And the response status code is 200
		And I request the organization "organization 1"
		Then the response status code is 200
		And the response body has a "id" field
		And the response body has a "licenses.free" field equal to number "7"
