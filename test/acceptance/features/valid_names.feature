Feature: Special characters on names
	As an api client
	I want to be able to send special characters on names

	Background:
		Given I am the user 1

	Scenario Outline: valid names on creation
		When I request creation of new organization with following data
			| name   | avatar   |
			| <NAME> | avatar 1 |
		Then the response status code is 201

		Examples:
			| NAME      |
			| AaAa 1    |
			| asdf-sadf |
			| asdf_sadf |
			| asdf.sadf |

	Scenario Outline: invalid names on creation
		When I request creation of new organization with following data
			| name   | avatar   |
			| <NAME> | avatar 1 |
		Then the response status code is 409
		And the response body has a "err" field with a "invalid_organization" value
		And the response body has a "des" field with a "invalid character on name" value

		Examples:
			| NAME         |
			| /            |
			| \            |
			| >            |
			| <            |

	Scenario Outline: valid names on edit
		When I request creation of new organization with following data
			| name           | avatar   |
			| organization 1 | avatar 1 |
		And I request to edit the organization "organization 1" to the new data
			| name   | avatar            |
			| <NAME> | http://org1_2.jpg |
		Then the response status code is 200

		Examples:
			| NAME      |
			| AaAa 1    |
			| asdf-sadf |
			| asdf_sadf |
			| asdf.sadf |

	Scenario Outline: invalid names on edit
		When I request creation of new organization with following data
			| name           | avatar   |
			| organization 1 | avatar 1 |
		And I request to edit the organization "organization 1" to the new data
			| name   | avatar            |
			| <NAME> | http://org1_2.jpg |
		Then the response status code is 409
		And the response body has a "err" field with a "invalid_organization" value
		And the response body has a "des" field with a "invalid character on name" value

		Examples:
			| NAME         |
			| /            |
			| \            |
			| >            |
			| <            |
