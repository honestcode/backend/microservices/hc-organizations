'use strict';

// module.exports = function () {
// 	this.Given(/^I am the user "([^"]*)"$/, (userName, done) => {

const {Given} = require('cucumber');

	Given(/^I am the user "([^"]*)"$/, function (userName) {
		const world = this.world;
		world.users = world.users || [];

		const user = world.users.find(user => user.name === userName);
		const userId = user ? user.id : 'invalid';

		world.requester = world.requester || {};
		world.requester.id = userId;
	});

