'use strict';

//const request = require('request');

// module.exports = function () {
//
// 	this.Given(/^I am the user (\d+)$/, (userId, done) => {

const {Given} = require('cucumber');

	Given(/^I am the user (\d+)$/, function (userId) {
		const world = this.world;
		world.requester = world.requester || {};
		world.requester.id = userId;
	});
// };
