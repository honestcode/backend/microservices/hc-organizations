'use strict';

// module.exports = function () {
//
// 	this.Given(/^The following users exists$/, (table, done) => {


const {Given} = require('cucumber');

	Given(/^The following users exists$/, function (table) {
		const world = this.world;
		world.users = world.users || [];

		return table.hashes().map(row => {
			world.users.push({
				id: row.id,
				name: row.name
			})
		});
	});
