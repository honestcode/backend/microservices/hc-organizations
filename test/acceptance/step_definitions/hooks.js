'use strict';
const {After, Before, setDefaultTimeout} = require('cucumber');

const httpServices = require('../../../src/http_services');
const dbConn = require('../../../src/database/connection');

const cleanDB = () => dbConn().then(db => db.dropDatabase());

setDefaultTimeout(15 * 1000);

Before(function () {
		this.world = {
			host : 'http://localhost:3002'
		};
		return httpServices
			.start(3002)
			.then(cleanDB);
});


After(function () {
		return httpServices
			.stop()
			.then(cleanDB);
});

