'use strict';

const expect = require('chai').expect;

function getObjectValue (object, path) {
	let value = object;
	const fields = path.split('.');

	fields.forEach(fieldName => {
		value = value[fieldName];
	});

	return value;
}

// module.exports = function () {
// 	this.Then(/^the response body has a "([^"]+)" field equal to (number|string) "([^"]+)"$/, (propertyPath, type, expectedValue) => {

const {Then} = require('cucumber');

	Then(/^the response body has a "([^"]+)" field equal to (number|string) "([^"]+)"$/, function (propertyPath, type, expectedValue) {

		const objectValue = getObjectValue(this.world.lastResponse.body, propertyPath);

		let typedExpectedValue;
		switch (type) {
			case 'number':
				typedExpectedValue = Number(expectedValue);
				break;
			case 'string':
				typedExpectedValue = expectedValue;
				break;
			default:
				throw new Error(`The type[${type}] is unknown`);
		}

		expect(objectValue).to.be.equal(typedExpectedValue);
	});
// };
