"use strict";

const assert = require('assert');

// module.exports = function () {
// 	this.Then(/^the response body has a "([^"]*)" field with (\d+) items?$/, (fieldName, itemCount, done) => {
const {Then} = require('cucumber');

	Then(/^the response body has a "([^"]*)" field with (\d+) items?$/, function (fieldName, itemCount) {

		const world = this.world;

		//console.log('world.lastResponse.body ->', world.lastResponse.body);
		assert.ok(world.lastResponse.body.hasOwnProperty(fieldName));
		assert.ok(world.lastResponse.body[fieldName].length === Number(itemCount));

		// done();
	});
// };
