"use strict";

const assert = require('assert');

// module.exports = function () {
// 	this.Then(/^the response body has a "([^"]*)" field with a "([^"]*)" value$/, (fieldName, fieldValue, done) => {

const {Then} = require('cucumber');

	Then(/^the response body has a "([^"]*)" field with a "([^"]*)" value$/, function (fieldName, fieldValue) {

		const world = this.world;

		assert.equal(world.lastResponse.body[fieldName] === undefined, false);
		assert.equal(world.lastResponse.body[fieldName].toString() === fieldValue.toString(), true);

		// done();
	});
//};
