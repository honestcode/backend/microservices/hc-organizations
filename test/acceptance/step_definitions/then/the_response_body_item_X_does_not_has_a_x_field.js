"use strict";

const assert = require('assert');

// module.exports = function () {
// 	this.Then(/^the response body (item|member) (\d+) does not has a "([^"]*)" field?$/, (arrayName, itemIndex, fieldName, done) => {


const {Then} = require('cucumber');

	Then(/^the response body (item|member) (\d+) does not has a "([^"]*)" field?$/, function (arrayName, itemIndex, fieldName) {

		const world = this.world;

		assert.ok(world.lastResponse.body.hasOwnProperty(`${arrayName}s`));
		assert.ok(!world.lastResponse.body[`${arrayName}s`][Number(itemIndex)].hasOwnProperty(fieldName));

		// done();
	});
// };
