"use strict";

const assert = require('assert');

// module.exports = function () {
// 	this.Then(/^the response body item (\d+) has a "([^"]*)" field list with (\d+) items$/, (itemIndex, fieldName, itemsNumber, done) => {

const {Then} = require('cucumber');

	Then(/^the response body item (\d+) has a "([^"]*)" field list with (\d+) items$/, function (itemIndex, fieldName, itemsNumber) {
		const world = this.world;

		assert.ok(world.lastResponse.body.hasOwnProperty("items"));
		assert.ok(world.lastResponse.body.items[Number(itemIndex)].hasOwnProperty(fieldName));
		assert.ok(world.lastResponse.body.items[Number(itemIndex)][fieldName].length === Number(itemsNumber));

		// done();
	});
// };
