"use strict";

const assert = require('assert');

//module.exports = function () {
	// this.Then(/^the response body (item|member) (\d+) has a "([^"]*)" field with (true|false) value?$/, (arrayName, itemIndex, fieldName, fieldValue, done) => {

const {Then} = require('cucumber');

	Then(/^the response body (item|member) (\d+) has a "([^"]*)" field with (true|false) value?$/, function (arrayName, itemIndex, fieldName, fieldValue) {

	 	const world = this.world;
		assert.ok(world.lastResponse.body.hasOwnProperty(`${arrayName}s`));
		assert.ok(world.lastResponse.body[`${arrayName}s`][Number(itemIndex)].hasOwnProperty(fieldName));
		assert.ok(world.lastResponse.body[`${arrayName}s`][Number(itemIndex)][fieldName] === (fieldValue === 'true'));

		// done();
	});
// };
