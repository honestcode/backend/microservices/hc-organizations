"use strict";

const assert = require('assert');

// module.exports = function () {
// 	this.Then(/^the response body (item|member|license) (\d+) has a "([^"]*)" field with "([^"]*)" value?$/, (arrayName, itemIndex, fieldName, fieldValue, done) => {
const {Then} = require('cucumber');

	Then(/^the response body (item|member|license) (\d+) has a "([^"]*)" field with "([^"]*)" value?$/, function (arrayName, itemIndex, fieldName, fieldValue) {
		const world = this.world;

		assert.ok(world.lastResponse.body.hasOwnProperty(`${arrayName}s`));
		assert.ok(world.lastResponse.body[`${arrayName}s`][Number(itemIndex)].hasOwnProperty(fieldName));
		assert.ok(world.lastResponse.body[`${arrayName}s`][Number(itemIndex)][fieldName].toString() === fieldValue.toString());

		// done();
	});
// };
