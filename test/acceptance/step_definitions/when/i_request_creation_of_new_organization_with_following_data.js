'use strict';

const request = require('request-promise');

// module.exports = function () {
//
// 	this.When(/^I request creation of new organization with following data$/,  (table, done) => {

const {When} = require('cucumber');

	When(/^I request creation of new organization with following data$/, function (table) {

		const world = this.world;
		world.organizations = world.organizations || [];

		const creatorId = world.requester.id.toString()
		const organizationData = table.hashes()[0];
		const organizationToCreate = {
			name: organizationData.name,
			avatar: organizationData.avatar,
			creatorId: creatorId
		};
/*
		const licenses = {};
		if (organizationData.hasOwnProperty('free')) licenses[0].free = organizationData.free;
		if (organizationData.hasOwnProperty('paid')) licenses[0].paid = organizationData.paid;
		if (organizationData.hasOwnProperty('expiresAt')) licenses[0].expiresAt = organizationData.expiresAt;
		if (Object.keys(licenses[0]).length) organizationToCreate.licenses = licenses;
*/
		world.organizations.push(organizationToCreate);

		const options = {
			url: world.host + '/api/organization',
			headers: {
				'x-transaction-id': 'tests',
				'x-debug': true
			},
			method: 'POST',
			json: true,
			body: organizationToCreate,
			resolveWithFullResponse: true
		};

		return request (options)
			.then(function (res) {
				world.lastResponse = {
					statusCode: res.statusCode,
					body: res.body
				};
				if (res.statusCode === 201) organizationToCreate.id = res.body.id;
			})
			.catch(function (error) {
				world.lastResponse = {
					statusCode: error.response.statusCode,
					body: error.response.body,
					error
				};
    		});

		// request(options, (error, res) => {
		// 	world.lastResponse = {
		// 		statusCode: res.statusCode,
		// 		body: res.body,
		// 		error
		// 	};

//			done();
		});
