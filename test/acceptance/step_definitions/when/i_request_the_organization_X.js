'use strict';

const {When} = require('cucumber');

//const request = require('request');
const request = require('request-promise');

// module.exports = function () {
// 	this.When(/^I request the organization "([^"]*)"$/, (name, done) => {

	When(/^I request the organization "([^"]*)"$/, function (name) {

		const world = this.world;
		world.organizations = world.organizations || [];

		const organization = world.organizations.find(organization => organization.name === name);
		const organizationId = organization ? organization.id : 'invalid';

		const options = {
			url: world.host + '/api/organization/' + organizationId,
			headers: {
				'x-transaction-id': 'tests',
				'x-debug': true
			},
			method: 'GET',
			json: true,
			resolveWithFullResponse: true
		};

		return request (options)
			.then(function (res) {
				world.lastResponse = {
					statusCode: res.statusCode,
					body: res.body
				};
			})
			.catch(function (error) {
				world.lastResponse = {
					statusCode: error.response.statusCode,
					body: error.response.body,
					error
				};
    		});


			// 	request("", options, (error, res) => {
			// 		/*
			// 					const licenses = [{}];
			// 					if (organization && organization.licenses) {
			// 						licenses[0].free = organization.licenses[0].hasOwnProperty('free') ? organization.licenses[0].free : 5;
			// 						licenses[0].paid = organization.licenses[0].hasOwnProperty('paid') ? organization.licenses[0].paid : 0;
			// 						licenses[0].expiresAt =organization.licenses[0].hasOwnProperty('expiresAt') ? organization.licenses[0].expiresAt : Date.now();
			// 						res.body.licenses = licenses;
			// 					}
			// 		*/
			//
			// 		world.lastResponse = {
			// 			statusCode: res.statusCode,
			// 			body: res.body,
			// 			error
			// 		};
			// 	});
			// })

	});
//};
