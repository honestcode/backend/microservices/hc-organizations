'use strict';

const request = require('request-promise');

// module.exports = function () {
//
// 	this.When(/^I request to add the following email "([^"]*)" as new member to organization "([^"]*)"$/, (email, name, done) => {

const {When} = require('cucumber');

	When(/^I request to add the following email "([^"]*)" as new member to organization "([^"]*)"$/, function (email, name) {

		const world = this.world;
		world.organizations = world.organizations || [];

		const organization = world.organizations.find(organization => organization.name === name);
		const organizationId = organization ? organization.id : 'invalid';

		//check user
		// add user

		const members = [{
			id: email
		}];

		const options = {
			url: world.host + `/api/organization/${organizationId}/members`,
			headers: {
				'x-transaction-id': 'tests',
				'x-debug': true
			},
			method: 'POST',
			json: true,
			body: {members},
			resolveWithFullResponse: true
		};

		return request(options)
			.then((res) => {
				world.lastResponse = {
					statusCode: res.statusCode,
					body: res.body
				};
			})
			.catch(res => {
				world.lastResponse = {
					statusCode: res.statusCode,
					body: res.error
				};
			});
	});
