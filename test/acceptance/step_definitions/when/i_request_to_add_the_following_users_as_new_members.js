'use strict';

const request = require('request-promise');

// module.exports = function () {
//
// 	this.When(/^I request to add the following users as new members to organization "([^"]*)"$/, (name, table, done) => {

const {When} = require('cucumber');

	When(/^I request to add the following users as new members to organization "([^"]*)"$/, function (name, table) {
		const world = this.world;
		world.organizations = world.organizations || [];

		const organization = world.organizations.find(organization => organization.name === name);
		const organizationId = organization ? organization.id : 'invalid';

		const members = table.hashes().map(row => {
			const member = {
				id: row.user
			};
			if (row.hasOwnProperty('isAdmin')) {
				member.isAdmin = row.isAdmin === 'true';
			}
			return member;
		});

		const options = {
			url: world.host + `/api/organization/${organizationId}/members`,
			headers: {
				'x-transaction-id': 'tests',
				'x-debug': true
			},
			method: 'POST',
			json: true,
			body: {members},
			resolveWithFullResponse: true
		};

		return request(options)
			.then((res) => {
				world.lastResponse = {
					statusCode: res.statusCode,
					body: res.body
				};
				// done();
			})
			.catch(res => {
				world.lastResponse = {
					statusCode: res.statusCode,
					body: res.error
				};
				// done();
			});
	});
// };
