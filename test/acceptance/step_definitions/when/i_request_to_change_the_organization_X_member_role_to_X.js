'use strict';

const request = require('request-promise');

// module.exports = function () {
//
// 	this.When(/^I request to change the organization "([^"]*)" member "([^"]*)" role to (admin|editor)$/, (organizationName, memberName, role, done) => {

const {When} = require('cucumber');

	When(/^I request to change the organization "([^"]*)" member "([^"]*)" role to (admin|editor)$/, function (organizationName, memberName, role) {
		const world = this.world;
		world.organizations = world.organizations || [];

		const organization = world.organizations.find(organization => organization.name === organizationName);
		const organizationId = organization ? organization.id : 'invalid';

		const member = world.users.find(user => user.name === memberName);
		const memberId = member ? member.id : 'invalid';

		const options = {
			url: `${world.host}/api/organization/${organizationId}/member/${memberId}`,
			headers: {
				'x-transaction-id': 'tests',
				'x-debug': true
			},
			method: 'PUT',
			json: true,
			body: {
				isAdmin: role === 'admin'
			},
			resolveWithFullResponse: true
		};

		return request (options)
			.then(function (res) {
				world.lastResponse = {
					statusCode: res.statusCode,
					body: res.body
				};
			})
			.catch(function (error) {
				world.lastResponse = {
					statusCode: error.response.statusCode,
					body: error.response.body,
					error
				};
    		});

// 		request(options, (error, res, body)=> {
// 			world.lastResponse = {
// 				statusCode: res.statusCode,
// 				body,
// 				error
// 			};
// //			done();
// 		});

	});
//};
