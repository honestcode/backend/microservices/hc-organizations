'use strict';

const request = require('request-promise');

// module.exports = function () {
//
// 	this.When(/^I request to delete the member with id (\d+) of the organization "([^"]*)"$/, (memberId, name, done) => {

const {When} = require('cucumber');

	When(/^I request to delete the member with id (\d+) of the organization "([^"]*)"$/, function (memberId, name) {

		const world = this.world;
		world.organizations = world.organizations || [];

		const organization = world.organizations.find(organization => organization.name === name);
		const organizationId = organization ? organization.id : 'invalid';

		const options = {
			url: `${world.host}/api/organization/${organizationId}/member/${memberId}`,
			headers: {
				'x-transaction-id': 'tests',
				'x-debug': true
			},
			method: 'DELETE',
			json: true,
			resolveWithFullResponse: true
		};

		return request (options)
			.then(function (res) {
				world.lastResponse = {
					statusCode: res.statusCode,
					body: res.body
				};
			})
			.catch(function (error) {
				world.lastResponse = {
					statusCode: error.response.statusCode,
					body: error.response.body,
					error
				};
    		});

		// request(options, (error, res, body)=> {
		// 	world.lastResponse = {
		// 		statusCode: res.statusCode,
		// 		body,
		// 		error
		// 	};
		// 	done();
		// });

	});
// };
