'use strict';

const request = require('request-promise');

// module.exports = function () {
//
// 	this.When(/^I request to delete the organization "([^"]*)"$/, (name, done) => {

const {When} = require('cucumber');

	When(/^I request to delete the organization "([^"]*)"$/, function (name) {
		const world = this.world;
		world.organizations = world.organizations || [];

		const organization = world.organizations.find(organization => organization.name === name);
		const organizationId = organization ? organization.id : 'invalid';

		const options = {
			url: world.host + `/api/organization/${organizationId}`,
			headers: {
				'x-transaction-id': 'tests',
				'x-debug': true
			},
			method: 'DELETE',
			json: true,
			resolveWithFullResponse: true
		};

		return request (options)
			.then(function (res) {
				world.lastResponse = {
					statusCode: res.statusCode,
					body: res.body
				};
			})
			.catch(function (error) {
				world.lastResponse = {
					statusCode: error.response.statusCode,
					body: error.response.body,
					error
				};
    		});

		// request(options)
		// 	.then(res => {
		// 		world.lastResponse = {
		// 			statusCode: res.statusCode,
		// 			body: res.body
		// 		};
		// 		// done();
		// 	})
		// 	.catch(err=> {
		// 		// done(err);
		// 	});

	});
// };
