'use strict';

const request = require('request-promise');

// module.exports = function () {
//
// 	this.When(/^I request to edit the organization "([^"]*)" to the new data$/, (name, table, done) => {

const {When} = require('cucumber');

	When(/^I request to edit the organization "([^"]*)" to the new data$/, function (name, table) {
		const world = this.world;
//		console.log('world.organizations ->', world.organizations);
		world.organizations = world.organizations || [];

		const organization = world.organizations.find(organization => organization.name === name);
		const organizationId = organization ? organization.id : 'invalid';

		const newData = table.hashes()[0];
		const updateData = {
			name: newData.name,
			avatar: newData.avatar,
			licenses: {
				free: Number(newData.licenses)
			}
		};

		const options = {
			url: world.host + '/api/organization/' + organizationId,
			headers: {
				'x-transaction-id': 'tests',
				'x-debug': true
			},
			method: 'PUT',
			json: true,
			body: updateData,
			resolveWithFullResponse: true
		};

		return request (options)
			.then(function (res) {
				world.lastResponse = {
					statusCode: res.statusCode,
					body: res.body
				};
				if (organization) Object.assign(organization, updateData);
			})
			.catch(function (error) {
				world.lastResponse = {
					statusCode: error.response.statusCode,
					body: error.response.body,
					error
				};
    		});

		// request(options, (err, res)=> {
		// 	world.lastResponse = {
		// 		statusCode: res.statusCode,
		// 		body: res.body
		// 	};
		// 	if (err) {
		// 		world.lastResponse.body = err;
		// 	}
		// 	if (organization) Object.assign(organization, updateData);
		// 	// done();
		// })

	});
// };
