'use strict';

const request = require('request-promise');
//
// module.exports = function () {
//
// 	this.When(/^I as (admin|member) request to find all organizations$/, (roles, done) => {

const {When} = require('cucumber');

	When(/^I as (admin|member) request to find all organizations$/, function (roles) {

		const world = this.world;
		let teamsQuery = (roles==="member") ? {memberId: world.requester.id} : null;

		const options = {
			url: world.host + '/api/organizations',
			headers: {
				'x-transaction-id': 'tests',
				'x-debug': true,
				'x-profile-id': world.requester.id
			},
			method: 'GET',
			json: true,
			resolveWithFullResponse: true,
			qs: {q: teamsQuery}
		};

		return request (options)
			.then(function (res) {
				world.lastResponse = {
					statusCode: res.statusCode,
					body: res.body
				};
			})
			.catch(function (error) {
				world.lastResponse = {
					statusCode: error.response.statusCode,
					body: error.response.body,
					error
				};
    		});

		// return request(options)
		// 	.then((res)=> {
		// 		world.lastResponse = {
		// 			statusCode: res.statusCode,
		// 			body: res.body
		// 		};
		// 	})
		// 	.catch(err=> {
		// 	});

	});
// };
