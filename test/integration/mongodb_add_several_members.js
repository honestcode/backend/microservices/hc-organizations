'use strict';

const createOrganization = require('../../src/organizations/create');
const addMembers = require('../../src/organizations/members_add');
const retrieveOrganization = require('../../src/organizations/retrieve');
const assert = require('assert');

const dbConn = require('../../src/database/connection');

const cleanDB = () => dbConn().then(db => db.dropDatabase());

describe('MongoDB add several members', function () {
	this.timeout(3000);

	before(cleanDB);

	after(cleanDB);

	it('Should add several members into an organization', function (done) {
		const membersToAdd = [{ id : 1},{id:2},{id:3, isAdmin : true}];
		let organizationId = null;

		createOrganization('Test Organization', 'Test avatar', 'creatorId')
			.then(createdOrganization => {
				organizationId = createdOrganization.id;
				return addMembers(organizationId, membersToAdd);
			})
			.then(() => retrieveOrganization(organizationId))
			.then(organization => {
				membersToAdd.splice(0, 0, { id : 'creatorId', isAdmin : true});

				assert.deepEqual(organization.members, membersToAdd);
				done();
			})
			.catch(done);
	});

});
